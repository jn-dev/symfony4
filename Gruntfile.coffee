module.exports = (grunt) ->

# Import #####################################################################

  require('jit-grunt')(grunt)

  grunt.loadTasks 'grunt-tasks'

  # Konfigurace ################################################################

  webBasePath = (file) ->
    output = 'components/web'
    if file
      output += '/' + file
    output

  cmsBasePath = (file) ->
    output = 'components/cms'
    if file
      output += '/' + file
    output

  grunt.initConfig

    uglify:
      web:
        options:
          compress:
            drop_console: true
          mangle: true
        files:
          'public/scripts.js': 'public/scripts.js'

      cms:
        options:
          compress:
            drop_console: true
          mangle: true
        files:
          'public/cms/scripts.js': 'public/cms/scripts.js'

    watch:
      options:
        spawn: false

      webStyles:
        files: ['components/web/**/*.less']
        tasks: ['less:web', 'postcss:web']

      webScripts:
        files: ['components/web/**/script.js']
        tasks: ['buildScripts:web']

      cmsStyles:
        files: ['components/cms/**/*.less']
        tasks: ['less:cms', 'postcss:cms']

      cmsScripts:
        files: ['components/cms/**/script.js']
        tasks: ['buildScripts:cms']

    less:
      web:
        files:
          'public/styles.css': 'components/web/main.less'
      cms:
        files:
          'public/cms/styles.css': 'components/cms/main.less'

    cssnano:
      options:
        discardComments:
          removeAll: true
      dist:
        files:
          'public/styles.css': 'public/styles.css'
          'public/cms/styles.css': 'public/cms/styles.css'

    postcss:
      options:
        processors: [
          require('autoprefixer')({
            browsers: ['last 3 versions', 'ios 6', 'ie >= 8']
          })
        ]
      web:
        src:
          'public/styles.css'
      cms:
        src:
          'public/cms/styles.css'

    buildGraphics:
      dist:
        options:
          imagesPath: 'public/images'
          outputScriptFile: 'public/graphics.js'
          objectName: "Graphics"

    dependenciesBuilder:
      web:
        options:
          basePath: webBasePath()
          vendorPathFromBasePath: '.'
          outputScriptsFile: webBasePath('main.js')
      cms:
        options:
          basePath: cmsBasePath()
          vendorPathFromBasePath: '.'
          outputScriptsFile: cmsBasePath('/main.js')

    buildScripts:
      web:
        options:
          basePath: webBasePath()
          dependenciesScriptsFile: webBasePath('main.js')
          outputScriptsFile: 'public/scripts.js'
#          finalJsCallback: '$(Document.callInit);'

      cms:
        options:
          basePath: cmsBasePath()
          dependenciesScriptsFile: cmsBasePath('main.js')
          outputScriptsFile: 'public/cms/scripts.js'

  # Úlohy ######################################################################

  grunt.registerTask 'init', [
    'dependenciesBuilder'
    'less'
    'postcss'
    'buildScripts'
  ]

  grunt.registerTask 'production', [
    'init'
    'cssnano'
    'uglify'
    'buildGraphics'
  ]

  grunt.registerTask 'development', [
    'init'
    'watch'
  ]


  # Aliasy #####################################################################

  grunt.registerTask 'dev', ->
    grunt.task.run 'development'

  grunt.registerTask 'build', ->
    grunt.task.run 'production'