<?php

namespace App\Form;

use App\Entity\CategoryEntity;
use App\Entity\ProductEntity;
use App\Entity\UserEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserFormType
 * @package App\Form
 */
class UserFormType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => UserEntity::class,
            )
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, ['label' => '_name'])
            ->add('roles', null, ['label' => '_roles'])
            ->add('password', null, ['label' => '_password'])->setRequired(false)
            ->add('save', SubmitType::class, ['label' => '_save']);
    }
}