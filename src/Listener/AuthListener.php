<?php

namespace App\Listener;

use App\AddOn\ExceptionHandler\ExceptionController;
use App\AddOn\FlashMessage\Service\FlashMessageService;
use App\Service\AuthService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthListener
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FlashMessageService
     */
    private $flashMessageService;

    /**
     * AuthListener constructor.
     *
     * @param RouterInterface $router
     * @param TokenStorageInterface $tokenStorage
     * @param FlashMessageService $flashMessageService
     */
    public function __construct(
        RouterInterface $router,
        TokenStorageInterface $tokenStorage,
        FlashMessageService $flashMessageService
    ) {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->flashMessageService = $flashMessageService;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        /** @var Request $request */
        $request = $event->getRequest();
        $routeName = $request->get("_route");

        /** @var \Symfony\Bundle\FrameworkBundle\Controller\Controller */
        $controller = $event->getController();
        $controller = $controller[0];

        if ($controller instanceof ExceptionController) {
            return;
        }

        if ($routeName === null) {
            return;
        }

        if (strpos($routeName, "_profiler") !== false) {
            return;
        }

        if (strpos($routeName, "_wdt") !== false) {
            return;
        }

        if (strpos($routeName, "_twig") !== false) {
            return;
        }

        $route = $this->router->getRouteCollection()->get($routeName);
        $routeRoles = $route->getDefault('_roles');

        if (empty($routeRoles)) {
            throw new \Exception("Route '$routeName' has not set '_roles' parameter for ACL ");
        }

        $user = $this->tokenStorage->getToken()->getUser();

        //user je prihlasen

        if ($user instanceof UserInterface) {
            foreach ($user->getRoles() as $role) {
                if (in_array($role, $routeRoles)) {
                    return;
                }
            }
        } else {
            if (in_array(AuthService::ROLE_ANONYMOUS, $routeRoles)) {
                return;
            }
        }

        $this->flashMessageService->createErrorMessage('not.access.permissions');

        //pokud nemam prava presmerovavam na login
        $redirectUrl = $this->router->generate('cms_authentication_login');

        $event->setController(
            function () use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
            }
        );

    }


}