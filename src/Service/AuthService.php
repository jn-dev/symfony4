<?php

namespace App\Service;

use App\Entity\UserEntity;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthService
{
    CONST AREA_CMS = 'cms';
    CONST AREA_WEB = 'web';

    CONST ROLE_ANONYMOUS = 'ANONYMOUS'; //USER NEMUIS BYT PRIHLASENY
    CONST ROLE_USER = 'USER';
    CONST ROLE_ADMIN = 'ADMIN';
    CONST ROLE_SUPERADMIN = 'SUPERADMIN';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * AuthService constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param UserPasswordEncoderInterface $encoder
     * @param UserRepository $userRepository
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        UserPasswordEncoderInterface $encoder,
        UserRepository $userRepository
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->encoder = $encoder;
        $this->userRepository = $userRepository;
    }

    public function getCurrentUser()
    {
        return $this->tokenStorage->getToken()->getUser() instanceof  UserEntity;
    }

    /** {@inheritdoc} */
    public function logout()
    {
        $this->tokenStorage->setToken();
    }

    /**
     * Base login action, authorize user to application by deffined area frontend / backend
     *
     * @param $username
     * @param $password
     * @param $providerKey
     *
     * @return void
     */
    public function login($username, $password, $providerKey)
    {

        try {
            /** @var null|UserEntity $user */
            $user = $this->userRepository->loadUserByUsername($username);
            if (!$user) {
                throw new AuthenticationException('User not found');
            }

            $passwordValid = $this->encoder->isPasswordValid($user, $password);
            if (!$passwordValid) {
                throw new AuthenticationException('Invalid password');
            }

            $this->tokenStorage->setToken(new UsernamePasswordToken($user, '', $providerKey, $user->getRoles()));

        } catch (AuthenticationException $e) {
            throw new AuthenticationException('Invalid username or password');
        }

    }

    /**
     * Generate encoded password
     *
     * @param UserInterface $user
     * @param               $password
     *
     * @return string
     */
    public function encodePassword(UserInterface $user, $password)
    {
        return $this->encoder->encodePassword($user, $password);
    }
}