

var Snippet = {

    request: function (url, method, data, elem, successCallback, errorCallback) {

        $.ajax({
            method: method,
            data: data,
            url: url,
            complete: function(){ console.log ('complete')},
            error: function() {
                if(errorCallback !== undefined) {
                    errorCallback();
                }
                console.log('pri ajax requestu se neco posralo');
            },
            success: function(data){

                data = $(data);

                $(data[0].snippets).each(function (i, s) {
                    var snippet = $(s);
                    $($("[data-snippet-id='" + snippet.attr("data-snippet-id") + "']")).each(function (i, e) {
                        $(e).replaceWith(snippet)
                    })
                });

                if(successCallback !== undefined) {
                    successCallback();
                }
            }
        });

    },

    initHref: function() {
        $('body').on('click', '[data-toggle="snippet"]', function(e){
            var href = $(e.target).attr("href");

            Snippet.request(href, "GET");

            return false;
        });
    },

    init: function() {
        Snippet.initHref();
        //todo form
    }
}

$(document).ready(Snippet.init())