<?php

namespace App\AddOn\Snippet\Extension;
use App\AddOn\Snippet\Service\SnippetService;
use Twig\Environment;

/**
 * Class SnippetTwigExtension
 * @package App\AddOn\Snippet\Extension
 */
class SnippetTwigExtension extends \Twig_Extension
{
    /**
     * @var SnippetService
     */
    public $snippetService;

    /**
     * SnippetTwigExtension constructor.
     * @param SnippetService $snippetService
     */
    public function __construct(SnippetService $snippetService)
    {
        $this->snippetService = $snippetService;
    }

    /**
     * Return new functions
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('snippet', [$this, 'snippetFunction'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('snippetTemplate', [$this, 'snippetTemplateFunction'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * Create snippet
     *
     * @param string $snippetType
     * @param array $options
     * @param null $snippetNameSuffix
     *
     * @return null|string
     */
    public function snippetFunction(string $snippetType, array $options = [], $snippetNameSuffix = null)
    {
        return $this->snippetService->renderSnippet($snippetType, $options, $snippetNameSuffix);
    }


    /**
     * @return string
     */
    public function snippetTemplateFunction()
    {
        $loader = new \Twig_Loader_Array(
            array(
                'snippet' => file_get_contents(__DIR__.'/../template/snippet.html.twig'),
            )
        );

        $twig = new \Twig_Environment($loader);
        return $twig->render('snippet');
    }
}
