<?php

namespace App\AddOn\Snippet\Service;

use App\Service\ContextService;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler;
use Symfony\Component\HttpKernel\Fragment\FragmentHandler;
use Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Kernel;
use Twig\Environment;

class SnippetService
{
    const REDRAW_SNIPPET_TYPE = "snippetType";
    const REDRAW_SNIPPET_NAME_SUFFIX = "snippetSuffix";
    const REDRAW_SNIPPET_OPTIONS = "snippetOptions";
    const REDRAW_SNIPPET_PARAMS = "snippetParams";

    /** @var RequestStack */
    public $requestStack;

    /** @var FragmentHandler */
    public $handler;

    /** @var DelegatingEngine */
    public $templating;

    /** @var Session */
    public $session;

    /** @var Translator */
    public $translator;

    /** @var Router */
    public $router;

    /** @var Kernel */
    public $kernel;

    /**
     * @var array
     *
     * Snippet id + controller [
     *  self::SNIPPET_DATATABLE => ["controller" => "App\Addon\Datatable\Controller\DatatableController"],
     * ]
     */
    public $snippetTypeMap = [];

    /**
     * SnippetService constructor.
     * @param array              $snippetTypeMap
     * @param RequestStack       $requestStack
     * @param ContainerInterface $container
     */
    public function __construct(
        array $snippetTypeMap,
        RequestStack $requestStack,
        ContainerInterface $container
    ) {
        $this->kernel = $container->get('kernel');
        $this->snippetTypeMap = $snippetTypeMap;
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $snippetType
     * @param string $snippetNameSuffix
     * @param array  $options
     * @return JsonResponse
     * @throws \Exception
     */
    public function redrawSnippetAndSendResponse($snippetType, $snippetNameSuffix = null, $options = [])
    {
        return $this->sendSnippetsResponse(
            [$this->renderSnippet($snippetType, $options, $snippetNameSuffix)]
        );
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    private function sendSnippetsResponse($data)
    {
//        $messages = $this->session->getFlashBag()->all();
//        foreach ($messages as $type => $messagesType) {
//            foreach ($messagesType as $key => $message) {
//                $messages[$type][$key] = $this->translator->trans($message);
//            }
//        }

        $response = [
            "snippets" => $data,
//            "flashMessages" => $messages,
        ];

        return new JsonResponse($response);
    }

    /**
     * @param       $snippetType
     * @param array $snippetOptions
     * @param null  $snippetNameSuffix
     * @return string
     * @throws \Exception
     */
    public function renderSnippet($snippetType, array $snippetOptions = [], $snippetNameSuffix = null)
    {

        if (!array_key_exists($snippetType, $this->snippetTypeMap)) {
            throw new \Exception("Snippet type \"".$snippetType."\" is invalid");
        }

        $snippetName = $snippetType;

        if ($snippetNameSuffix) {
            $snippetName .= "--".$snippetNameSuffix;
        }

        $controller = $this->snippetTypeMap[$snippetType];

        $subRequest = new Request();
        $subRequest->attributes->set('_controller', $controller);
        $subRequest->attributes->set('snippetName', $snippetName);
        $subRequest->attributes->set('snippetType', $snippetName);
        $subRequest->attributes->set('snippetNameSuffix', $snippetName);

        foreach($snippetOptions as $key => $value) {
            $subRequest->attributes->set($key, $value);
        }


        $response = $this->kernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);

        if($response->isSuccessful()) {
            return $response->getContent();
        } else {
            echo $response->getContent();
            exit;
        }


    }

    /**
     * @param array $snippetsOptions
     * @return JsonResponse
     * @throws \Exception
     */
    public function redrawSnippetsAndSendResponse($snippetsOptions)
    {
        $data = [];

        foreach ($snippetsOptions as $snippetOption) {
            $snippetType = $snippetOption[self::REDRAW_SNIPPET_TYPE];
            $snippetNameSuffix = isset($snippetOption[self::REDRAW_SNIPPET_NAME_SUFFIX]) ?
                $snippetOption[self::REDRAW_SNIPPET_NAME_SUFFIX] : null;
            $options = isset($snippetOption[self::REDRAW_SNIPPET_OPTIONS]) ?
                $snippetOption[self::REDRAW_SNIPPET_OPTIONS] : [];

            $data[] = $this->renderSnippet($snippetType, $options, $snippetNameSuffix);
        }

        return $this->sendSnippetsResponse($data);
    }

    /**
     * @return bool
     */
    public function isSnippetRequest()
    {
        return $this->getSnippetName() ? true : false;
    }

    /**
     * @return mixed
     */
    public function getSnippetName()
    {
        return $this->getRequest(false)->get("snippetName");
    }

    /**
     * @param      $name
     * @param null $default
     * @return mixed
     */
    public function getParameter($name, $default = null)
    {
        return $this->getRequest(false)->get($name, $this->getRequest(true)->get($name, $default));
    }

    /**
     * Return injected return statement
     * @param array $return
     * @param null  $snippetName
     * @return array
     */
    public function returnResponse(array $return, $snippetName = null)
    {
        $return["snippetName"] = $snippetName ? $snippetName : $this->getSnippetName();

        return $return;
    }

    /**
     * @param bool $master
     * @return null|\Symfony\Component\HttpFoundation\Request
     */
    public function getRequest($master = true)
    {
        if ($master) {
            return $this->requestStack->getMasterRequest();
        }

        return $this->requestStack->getCurrentRequest();
    }

    /**
     * @param string $redirectUrl
     * @return JsonResponse
     */
    public function sendRedirectResponse($redirectUrl)
    {
        $response = [
            "redirect" => $redirectUrl,
        ];

        return new JsonResponse($response);
    }
}
