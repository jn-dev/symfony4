# Snippet

## Instalace

#### Uprava konfiguračních souborů:

config/packages/twig.yaml

```
twig:
  paths:
    '%kernel.project_dir%/src/AddOn/Snippet/template': SnippetTemplate
```


## Použití

SnippetController
- vraci return renderResponse([])
- v template {{ snippet(snippetName) }}
- v service definuju snipeptName associaci na controller
