<?php

namespace App\AddOn\Datatable\Service;

use Twig\Environment;

class DatatableService
{
    /** @var  Environment */
    protected $twig;

    /**
     * DatatableService constructor.
     */
    public function __construct(Environment $environment)
    {
        $this->twig =  $environment;
    }

    public function datatable($data)
    {
        return $this->twig->render("@DataTableTemplate/datatable.html.twig", $data);
    }
}