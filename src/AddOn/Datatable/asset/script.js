var DataTable = {
    vars: {
        selector: "[data-role='datatable']",
        searchSelector: "input[name='datatable[search]']",
        pageSelector: "[data-role='datatable-page']",
        limitSelector: "[data-role='datatable-limit']",
        orderSelector: "[data-role='datatable-order']",
        blockerSelector: ".datatable-blocker",
        timeout: 500,
    },
    init: function() {
        $(DataTable.vars.selector + ":not([data-init=true])").each(function(i,el){

            var datatable = $(el);
            datatable.attr("data-init", true);

            DataTable.initSearch(datatable);
            DataTable.initLimit(datatable);
            DataTable.initPage(datatable);
            DataTable.initOrder(datatable);

        })
    },

    initSearch: function(datatable) {

        var search = datatable.find(DataTable.vars.searchSelector).first();
        datatable.attr("data-search", "");

        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $('body').on('keyup', DataTable.vars.selector + " " + DataTable.vars.searchSelector ,function(event) {
            delay(function(){

                var search = $(event.target);
                var datatable = search.closest(DataTable.vars.selector);

                datatable.attr("data-search", search.val());

                //vyresetuju paginator
                datatable.attr("data-page", 1);

                DataTable.sendRequest(datatable);

            }, DataTable.vars.timeout );
        });
    },

    initPage: function(datatable) {

        //Default
        datatable.attr("data-page", 1);

        $('body').on('click', DataTable.vars.selector + " " + DataTable.vars.pageSelector ,function(event) {
            var pageItem = $(event.target);
            var page = pageItem.attr("href");
            var datatable = pageItem.closest(DataTable.vars.selector);
            datatable.attr("data-page", page);
            DataTable.sendRequest(datatable);

            return false;
        });

    },

    initOrder: function(datatable) {

        //Default
        datatable.attr("data-order", "");

        $('body').on('click', DataTable.vars.selector + " " + DataTable.vars.orderSelector ,function(event) {
            var orderItem = $(event.target);
            var order = orderItem.attr("data-order");
            var datatable = orderItem.closest(DataTable.vars.selector);
            datatable.attr("data-order", order);

            DataTable.sendRequest(datatable);

            return false;
        });
    },

    initLimit: function(datatable) {

        //Default
        var limitItem = datatable.find(DataTable.vars.limitSelector);
        var limit = limitItem.val();
        datatable.attr("data-limit", limit);

        $('body').on('change', DataTable.vars.selector + " " + DataTable.vars.limitSelector ,function(event) {
            var limitItem = $(event.target);
            var limit = limitItem.val();
            var datatable = limitItem.closest(DataTable.vars.selector);
            datatable.attr("data-limit", limit);

            DataTable.sendRequest(datatable);

            return false;
        });
    },

    sendRequest: function(datatable) {

        var url = datatable.attr("data-href");

        url = url.replace("__SEARCH-QUERY__", encodeURI(datatable.attr("data-search")));
        url = url.replace("__LIMIT__", encodeURI(datatable.attr("data-limit")));
        url = url.replace("__PAGE__", encodeURI(datatable.attr("data-page")));
        url = url.replace("__ORDER__", encodeURI(datatable.attr("data-order")));

        var blocker = datatable.find(DataTable.vars.blockerSelector);
        console.log(blocker);

        blocker.css({visibility:"visible"});
        blocker.animate({opacity: 1}, 200);

        Snippet.request(url, "GET");
    }
}

$(document).ready(DataTable.init());