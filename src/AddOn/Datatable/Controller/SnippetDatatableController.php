<?php

namespace App\AddOn\Datatable\Controller;

use App\AddOn\Datatable\Utils\Paginator as Pagination;
use App\AddOn\Snippet\Service\SnippetService;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DatatableController
 * @package App\AddOn\Datatable\Controller
 *
 */
class SnippetDatatableController
{
    /**
     * Vnitřní akce - používá se pro vykreslení sub-requestu
     *
     * @Template("@DataTableTemplate/datatable-snippet.html.twig")
     *
     * @param SnippetService    $snippetService
     * @param RegistryInterface $registry
     * @return array
     * @throws \Exception
     */
    public function template(SnippetService $snippetService, RegistryInterface $registry)
    {
        $repositoryName = $snippetService->getParameter("repository_name");
        $repositoryMethod = $snippetService->getParameter("repository_method");
        $repositoryMethodParams = $snippetService->getParameter("repository_method_params", []);
        $columns = $snippetService->getParameter("columns");
        $searchColumns = $snippetService->getParameter("searchColumns", []);
        $searchQuery = $snippetService->getParameter("searchQuery");
        $order = $snippetService->getParameter("order");
        $limit = $snippetService->getParameter("limit", 10);
        $page = $snippetService->getParameter("page", 1);

        $pagination = new Pagination();
        $pagination->setPage($page);
        $pagination->setPageLimit($limit);

        if (!$repositoryName) {
            throw new \Exception("RepositoryName must be set");
        }

        $repository = $registry->getRepository($repositoryName);

        if (!$repositoryMethod) {
            throw new \Exception("RepositoryMethod must be set");
        }

        if (!method_exists($repository, $repositoryMethod)) {
            $class = get_class($repository);
            throw new \Exception("RepositoryMethod '$repositoryMethod' is not callable in $class");
        }

        if (!$columns) {
            throw new \Exception("Columns must be set");
        }

        $queryBuilder = call_user_func_array([$repository, $repositoryMethod], $repositoryMethodParams);
        if (!$queryBuilder instanceof QueryBuilder) {
            throw new \Exception("Returned object must be instance of Doctrine\ORM\QueryBuilder.");
        }

        // Search query
        if (!empty($searchQuery)) {

            $exs = [];
            foreach ($searchColumns as $searchColumn) {
                $exs[] = $queryBuilder->expr()->like($searchColumn, ":search");
            }

            // do orX se musi volat jednotlive podminky jako argumenty omg :( proto call_user_func_array
            $expr = call_user_func_array([$queryBuilder->expr(), "orX"], $exs);

            $queryBuilder->andWhere($expr);
            $queryBuilder->setParameter("search", "%$searchQuery%");
        }

        //strankovani
        $queryBuilder
            ->setMaxResults($pagination->getPageLimit())
            ->setFirstResult($pagination->getOffset());

        //Vytahnu data
        $rows = new Paginator($queryBuilder);

        //Nasetuju celkovy pocet polozek pro vypocty
        $pagination->setTotalCount($rows->count());


        return $snippetService->returnResponse(
            [
                "repository_name" => $repositoryName,
                "repository_method" => $repositoryMethod,
                "repository_method_params" => $repositoryMethodParams,
                "columns" => $columns,
                "searchColumns" => $searchColumns,
                "searchQuery" => $searchQuery,
                "order" => $order,
                "limit" => $limit,
                "pagination" => $pagination,
                "rows" => $rows,
            ]
        );

    }

    /**
     * Provede přerenderování obsahu datatable
     *
     * @Route("/datatable/redraw", name="addOn_datatable_redraw")
     *
     * @param SnippetService $snippetService
     * @return JsonResponse
     */
    public function redraw(SnippetService $snippetService)
    {
        return $snippetService->redrawSnippetAndSendResponse(
            'datatable',
            null,
            [
                "repository_name" => $snippetService->getParameter("repository_name"),
                "repository_method" => $snippetService->getParameter("repository_method"),
                "repository_method_params" => $snippetService->getParameter("repository_method_params", []),
                "columns" => $snippetService->getParameter("columns", []),
                "searchColumns" => $snippetService->getParameter("searchColumns"),
                "searchQuery" => $snippetService->getParameter("searchQuery"),
                "page" => $snippetService->getParameter("page"),
                "order" => $snippetService->getParameter("order"),
                "limit" => $snippetService->getParameter("limit"),
            ]
        );
    }
}
