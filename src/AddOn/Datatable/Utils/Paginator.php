<?php

namespace App\AddOn\Datatable\Utils;

class Paginator
{
    /** @var int */
    private $page;

    /** @var int */
    private $pageLimit;

    /** @var int */
    private $totalCount;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return self
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageLimit(): int
    {
        return $this->pageLimit;
    }

    /**
     * @param int $pageLimit
     * @return self
     */
    public function setPageLimit($pageLimit)
    {
        $this->pageLimit = $pageLimit;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return self
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;

        return $this;
    }

    public function getOffset()
    {
        if ($this->page === null || $this->pageLimit === null) {
            throw new \Exception('PageLimit and page must by set');
        }

        return ($this->page - 1) * $this->pageLimit;
    }

    public function getPageCount()
    {
        if ($this->totalCount === null && $this->pageLimit === null) {
            throw new \Exception('PageLimit and totalCount must by set');
        }

        return ceil($this->totalCount / $this->pageLimit);
    }

}
