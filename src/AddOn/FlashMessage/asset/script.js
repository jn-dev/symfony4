/**
 * Created by jirka on 11.02.18.
 */
var FlashMessage = {
    vars: {
        selectorClose: '[data-flash-message="close"]',
        selector: '.flash-message',
        classError: 'flash-message--error',

        delayAutoCloseError: 5000,
        delayAutoClose: 4000
    },

    init: function () {
        FlashMessage.onCLickClose();

        $.each($(FlashMessage.vars.selector), function (index, message) {
            FlashMessage.autoClose($(message));
        })

    },

    onCLickClose: function () {
        $('body').on('click', FlashMessage.vars.selectorClose, function () {
            var message = $(this).parent();
            FlashMessage.removeMessage(message);
        })
    },

    removeMessage: function (message) {
        message.slideUp(function () {
            this.remove()
        });
    },

    autoClose: function (message) {
        var delay = FlashMessage.vars.delayAutoClose;
        if (message.hasClass(FlashMessage.vars.classError)) {
            delay = FlashMessage.vars.delayAutoCloseError;
        }

        setTimeout(function () {
            FlashMessage.removeMessage(message)
        }, delay);
    }
}

$(document).ready(FlashMessage.init);