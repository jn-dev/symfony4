<?php

namespace App\AddOn\FlashMessage\Service;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FlashMessageService
 * @package App\AddOn\FlashMessage\Service
 */
class FlashMessageService
{
    /** @var string */
    private static $infoMessage = "info";

    /** @var string */
    private static $successMessage = "success";

    /** @var string */
    private static $errorMessage = "error";

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * FlashMessageService constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Create info flashMessage
     *
     * @param string $message - Message without translation
     */
    public function createInfoMessage(string $message)
    {
        $this->createMessage(self::$infoMessage, $message);
    }

    /**
     * Create success flashMessage
     *
     * @param string $message - Message without translation
     */
    public function createSuccessMessage(string $message)
    {
        $this->createMessage(self::$successMessage, $message);
    }

    /**
     * Create error flashMessage
     *
     * @param string $message - Message without translation
     */
    public function createErrorMessage(string $message)
    {
        $this->createMessage(self::$errorMessage, $message);
    }

    /**
     * Create flashMessage
     *
     * @param string $type
     * @param string $message
     */
    private function createMessage(string $type, string $message)
    {
        if (!$this->container->has('session')) {
            throw new \LogicException(
                'You can not use the addFlash method if sessions are disabled. Enable them in "config/packages/framework.yaml".'
            );
        }

        if (!$this->container->has('translator')) {
            throw new \LogicException(
                'Translation is not enabled.'
            );
        }

        $this->container->get('session')->getFlashBag()->add(
            $type,
            $this->container->get('translator')->trans($message)
        );
    }
}