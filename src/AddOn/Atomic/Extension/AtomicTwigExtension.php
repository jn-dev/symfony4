<?php

namespace App\AddOn\Atomic\Extension;

/**
 * Class AtomicTwigExtension
 * @package App\Extension\Twig
 */
class AtomicTwigExtension extends \Twig_Extension
{
    /**
     * Return new functions
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('web_atom', [$this, 'includeWebAtomFunction']),
            new \Twig_SimpleFunction('web_molecule', [$this, 'includeWebMoleculeFunction']),
            new \Twig_SimpleFunction('web_organism', [$this, 'includeWebOrganismFunction']),
            new \Twig_SimpleFunction('web_template', [$this, 'includeWebTemplateFunction']),
            new \Twig_SimpleFunction('cms_atom', [$this, 'includeCmsAtomFunction']),
            new \Twig_SimpleFunction('cms_molecule', [$this, 'includeCmsMoleculeFunction']),
            new \Twig_SimpleFunction('cms_organism', [$this, 'includeCmsOrganismFunction']),
            new \Twig_SimpleFunction('cms_template', [$this, 'includeCmsTemplateFunction']),
            new \Twig_SimpleFunction('componentClass', [$this, 'componentClassFunction']),
            new \Twig_SimpleFunction('componentAttributes', [$this, 'componentAttributesFunction']),
        ];
    }

    /**
     * Include atom
     * @param string $componentName
     * @return string
     */
    public function includeWebAtomFunction(string $componentName): string
    {
        return $this->includeComponent('atoms/'.$componentName, 'web');
    }

    /**
     * Include molecule
     * @param string $componentName
     * @return string
     */
    public function includeWebMoleculeFunction(string $componentName): string
    {
        return $this->includeComponent('molecules/'.$componentName, 'web');
    }

    /**
     * Include organism
     * @param string $componentName
     * @return string
     */
    public function includeWebOrganismFunction(string $componentName): string
    {
        return $this->includeComponent('organisms/'.$componentName, 'web');
    }

    /**
     * Include template
     * @param string $componentName
     * @return string
     */
    public function includeWebTemplateFunction(string $componentName): string
    {
        return $this->includeComponent('templates/'.$componentName, 'web');
    }

    /**
     * Include atom
     * @param string $componentName
     * @return string
     */
    public function includeCmsAtomFunction(string $componentName): string
    {
        return $this->includeComponent('atoms/'.$componentName, 'cms');
    }

    /**
     * Include molecule
     * @param string $componentName
     * @return string
     */
    public function includeCmsMoleculeFunction(string $componentName): string
    {
        return $this->includeComponent('molecules/'.$componentName, 'cms');
    }

    /**
     * Include organism
     * @param string $componentName
     * @return string
     */
    public function includeCmsOrganismFunction(string $componentName): string
    {
        return $this->includeComponent('organisms/'.$componentName, 'cms');
    }

    /**
     * Include template
     * @param string $componentName
     * @return string
     */
    public function includeCmsTemplateFunction(string $componentName): string
    {
        return $this->includeComponent('templates/'.$componentName, 'cms');
    }

    /**
     * Class maker for components
     * @param string     $componentName Název CSS třídy komponenty.
     * @param array      $modifiers     Pole BEM modifikátorů.
     * @param array|null $classes       Pole rozšiřujících tříd.
     * @return string
     */
    public function componentClassFunction(string $componentName, array $modifiers, array $classes = []): string
    {
        $outputAttribute = $componentName;

        // Add BEM modificators for base component class
        if (is_array($modifiers) && count($modifiers)) {
            sort($modifiers);
            foreach ($modifiers as $modifier) {
                $outputAttribute = $outputAttribute.'  '.$componentName.'--'.$modifier;
            }
        }

        if (is_array($classes) && count($classes)) {
            sort($classes);
            foreach ($classes as $class) {
                $outputAttribute = $outputAttribute.'  '.$class;
            }
        }

        return $outputAttribute;
    }

    /**
     * Attribute maker for components
     * @param array $attributes
     * @return string
     */
    public function componentAttributesFunction(array $attributes): string
    {
        $data = '';
        if (is_array($attributes) && count($attributes)) {
            ksort($attributes);
            foreach ($attributes as $key => $value) {
                if ($value != false) {
                    $data .= ' '.$key;
                    if (!is_bool($value)) {
                        $data .= '="'.$value.'"';
                    }
                }
            }
        }

        return trim($data);
    }

    /**
     * Include component path
     * @param string $componentPath
     * @param string $section
     * @return string
     */
    private function includeComponent(string $componentPath, string $section): string
    {
        return "@Component/$section/$componentPath/template.html.twig";
    }
}
