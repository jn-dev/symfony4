<?php

namespace App\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController
{
    /**
     * Test controller
     *
     * @return array;
     *
     * @Route("", name="web_default", defaults={"_roles" = {App\Service\AuthService::ROLE_ANONYMOUS}})
     *
     * @Template("@WebTemplate/simple-page/template.html.twig")
     */
    public function index()
    {
        return [
            "text" => 'Hello world!',
        ];
    }
}