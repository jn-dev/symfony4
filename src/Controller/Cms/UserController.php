<?php

namespace App\Controller\Cms;

use App\AddOn\Authentication\Service\AuthenticationService;
use App\AddOn\Datatable\Service\DatatableService;
use App\AddOn\FlashMessage\Service\FlashMessageService;
use App\Entity\UserEntity;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use App\Service\AuthService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class UserController
 * @package App\Controller\Cms
 */
class UserController
{
    /**
     * @param DatatableService $datatableService
     * @param RouterInterface  $router
     *
     * @return array
     *
     * @Route("/cms/user", name="cms_user_default")
     * @Template("@CmsTemplate/datatable/template.html.twig")
     */
    public function default(DatatableService $datatableService, RouterInterface $router)
    {

        $alias = UserRepository::$rootQueryBuilderAlias;
        $data = [
            'repository_name' => UserRepository::getName(),
            'repository_method' => 'queryBuilderAll',
            'repository_method_params' => [],
            'searchColumns' => ["$alias.id", "$alias.name"],
            'newLink' => $router->generate('cms_user_new'),
            'newLabel' => 'add.new.user',
            'columns' => [
                'id' => [
                    'label' => '_id',
                    'order' => true,
                    'render' => '{{ row.id }}',
                ],
                'email' => [
                    'label' => '_email',
                    'render' => '{{ row.email }}',
                ],
                'roles' => [
                    'label' => '_roles',
                    'render' => '{{ row.roles }}',
                ],
                'action' => [
                    'label' => '_action',
                    'render' => "
                        {% include cms_atom('btn') with {
                            href: path('cms_user_edit', {id: row.id}),
                            classes: ['btn-primary'],
                            label: '_edit'|trans,
                        } %}
                    ",
                ],
            ],
        ];

        return [
            'heading' => 'users',
            'datatable' => $datatableService->datatable($data),
        ];
    }

    /**
     * Create / edit user
     *
     * @param Request              $request
     * @param UserRepository       $repository
     * @param FormFactoryInterface $factory
     * @param RouterInterface      $router
     * @param AuthService          $authService
     * @param FlashMessageService  $flashMessageService
     *
     * @return array|RedirectResponse
     *
     * @Route("/cms/user/new", name="cms_user_new")
     * @Route("/cms/user/{id}/edit", name="cms_user_edit", requirements={"id" = "\d+"})
     * @Template("@CmsTemplate/form/template.html.twig")
     */
    public function editAction(
        Request $request,
        UserRepository $repository,
        FormFactoryInterface $factory,
        RouterInterface $router,
        AuthService $authService,
        FlashMessageService $flashMessageService
    ) {
        $id = $request->get('id');

        $entity = new UserEntity();

        if ($id) {
            $findEntity = $repository->findOneById($id);
            if ($findEntity) {
                $entity = $findEntity;
            } else {
                throw new NotFoundHttpException();
            }
        }

        $originalPassword = $entity->getPassword();
        $entity->setPassword('');

        $form = $factory->create(UserFormType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $plainPassword = $form->get('password')->getData();

            if ($plainPassword) {
                $entity->setPassword($authService->encodePassword($entity, $plainPassword));
            } else {
                $entity->setPassword($originalPassword);
            }

            if ($entity->getId()) {
                $repository->update($entity);
                $flashMessageService->createSuccessMessage('user.was.updated');
            } else {
                $repository->insert($entity);
                $flashMessageService->createSuccessMessage('user.was.created');
            }

            $url = $router->generate('cms_user_default');

            return new RedirectResponse($url);
        }

        return [
            'heading' => $entity->getId() ? 'edit.user' : 'add.new.user',
            'form' => $form->createView(),
        ];
    }
}