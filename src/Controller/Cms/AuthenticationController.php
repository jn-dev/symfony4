<?php

namespace App\Controller\Cms;

use App\Form\LoginForm;
use App\Service\AuthService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Translation\Translator;

/**
 * Class AuthenticationController
 * @package App\BackendBundle\Controller
 */
class AuthenticationController
{
    /**
     * @var
     */
    public $authenticationUtils;

    /**
     * @var Translator
     * @Autowired
     */
    public $translator;

    /**
     * @param Request              $request
     * @param FormFactoryInterface $formFactory
     * @param AuthService          $authService
     * @param RouterInterface      $router
     *
     * @return array|RedirectResponse
     *
     * @Route("/cms/login", name="cms_authentication_login", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ANONYMOUS,
     *     App\Service\AuthService::ROLE_USER,
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     * @Template("@CmsTemplate/login/template.html.twig")
     */
    public function loginAction(
        Request $request,
        FormFactoryInterface $formFactory,
        AuthService $authService,
        RouterInterface $router
    ) {

        if ($authService->getCurrentUser()) {
            return new RedirectResponse($router->generate("cms_dashboard_default"));
        }

        $form = $formFactory->create(LoginForm::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();

                $authService->login($data['email'], $data["password"], AuthService::AREA_CMS);

                // Redirect bud na url v parametru next, nebo na default
                $redirectUrl = urldecode($request->get("next", $router->generate('cms_dashboard_default')));

                return new RedirectResponse($redirectUrl);

            } catch (AuthenticationException $e) {
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return [
            "form" => $form->createView()
        ];
    }

    /**
     * @param AuthService     $authService
     * @param RouterInterface $router
     *
     * @return RedirectResponse
     *
     * @Route("/cms/logout", name="cms_authentication_logout", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_USER,
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     */
    public function logout(AuthService $authService, RouterInterface $router)
    {
        $authService->logout();

        return new RedirectResponse($router->generate('cms_authentication_login'));
    }
}
