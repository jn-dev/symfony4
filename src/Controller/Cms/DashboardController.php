<?php

namespace App\Controller\Cms;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package App\Controller\Cms
 */
class DashboardController
{
    /**
     * @return array
     *
     * @Route("/cms/dashboard", name="cms_dashboard_default", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_USER,
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     * @Template("@CmsTemplate/dashboard/template.html.twig")
     */
    public function default()
    {
        return [];
    }
}