<?php

namespace App\Extension\Twig;

use App\TemplateService\AbstractTemplateService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AtomicTwigExtension
 * @package App\Extension\Twig
 */
class ServiceTwigExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ServiceTwigExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * Return new functions
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('service', [$this, 'getService']),
        ];
    }

    /**
     * Return TemplateService by name
     *
     * @param string $name
     * @return AbstractTemplateService
     * @throws \Exception
     */
    public function getService(string $name)
    {
        $name = ucfirst($name);
        $className = 'App\TemplateService\\'.$name.'TemplateService';

        if ($this->container->has($className)) {
            /** @var AbstractTemplateService $service */
            $service = $this->container->get($className);

            return $service;
        } else {
            throw new \Exception("Template service with alias '$name($className)' not found.");
        }
    }
}
