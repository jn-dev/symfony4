<?php

namespace App\Repository;

use App\Entity\UserEntity;

class UserRepository extends AbstractRepository
{
    public static $entityClass = UserEntity::class;
    public static $rootQueryBuilderAlias = 'u';


    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')

            ->where('u.email = :email')
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
