<?php

namespace App\Repository;

use App\Entity\AbstractEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractRepository extends ServiceEntityRepository
{
    public static $entityClass = null;
    public static $rootQueryBuilderAlias = null;

    /**
     * Get repository EntityClassName
     *
     * @return null|string
     */
    public static function getName()
    {
        $calledClass = get_called_class();

        return $calledClass::$entityClass;
    }

    /**
     * AbstractRepository constructor.
     *
     * @param RegistryInterface $registry
     *
     * @throws \Exception
     */
    public function __construct(RegistryInterface $registry)
    {
        $class = get_called_class();

        if (empty($class::$entityClass)) {
            $calledClass = get_called_class();
            throw new \Exception("Repository '$calledClass' has not set 'protected \$entityClass = Entity::class'.");
        }

        if (!class_exists($class::$entityClass)) {
            $calledClass = get_called_class();
            throw new \Exception(
                "Repository '$calledClass' call 'protected \$entityClass = $class', but this class not exists."
            );
        }

        if (empty($class::$rootQueryBuilderAlias)) {
            $calledClass = get_called_class();
            throw new \Exception("Repository '$calledClass' has not set 'protected \$rootQueryBuilderAlias = 'e'.");
        }

        parent::__construct($registry, $class::$entityClass);
    }

    /**
     * Create base QueryBuilder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function queryBuilderAll()
    {
        $class = get_called_class();

        return $this->createQueryBuilder($class::$rootQueryBuilderAlias);
    }

    /**
     * Find category by id
     *
     * @param int $id
     *
     * @return null|AbstractEntity
     */
    public function findOneById(int $id)
    {
        /** @var null|AbstractEntity $result */
        $result = $this->find($id);

        return $result;
    }

    /**
     * Update AbstractEntity
     *
     * @param AbstractEntity $entity
     *
     * @return AbstractEntity
     */
    public function update(AbstractEntity $entity)
    {
        $this->getEntityManager()->merge($entity);
        $this->getEntityManager()->flush($entity);

        return $entity;
    }

    /**
     * @param AbstractEntity $entity
     *
     * @return AbstractEntity
     */
    public function insert(AbstractEntity $entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        return $entity;
    }
}