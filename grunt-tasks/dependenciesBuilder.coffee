'use strict'

module.exports = (grunt) ->
  grunt.registerMultiTask 'dependenciesBuilder', 'The best Grunt plugin ever.', ->
    options = @options(
      basePath: undefined
      vendorPathFromBasePath: undefined
      outputScriptsFile: undefined

      finalJsCallback: ''

      lessDependenciesFilename: 'dependencies.less'
      styleVendorsFilename: 'vendors.less'
      jsonDependenciesFilename: 'dependencies.json'
    )

    fileExists = require('file-exists')
    del = require('del')
    writeFile = require('write')
    Finder = require('fs-finder')
    readFile = require('read-file')

    if typeof JSON.clone != 'function'
      JSON.clone = (obj) ->
        JSON.parse JSON.stringify(obj)

    rules = {
      "templates": ["vendor"]
      "organisms": ["vendor"]
      "molecules": ["vendor"]
      "atoms": ["vendor"]
      "base": ["vendor", "atoms"] # povolené sekce v kořenovém souboru závislostí
    }

    outputJson = {
      "templates": {}
      "organisms": {}
      "molecules": {}
      "atoms": {}
      "vendor": {}
    }

    structure = [
      "atoms"
      "molecules"
      "organisms"
      "templates"
    ]

    componentFiles = {
      vendor:
        scripts: /\.js$/
        styles: /\.css$/
      components:
        scripts: 'script.js'
        styles: 'style.less'
      lessDependencies: [
        "variables.less"
        "mixins.less"
        "style.less"
      ]
    }



    #Z namapovaneho output JSONU mi vrati cesty vendoru na zaklade pravidla (/*.css | /*.js)
    getVendorPaths = (pattern) ->
      vendors = JSON.clone(outputJson.vendor);
      for key,vendor of vendors
        if key.match(pattern)
          key
        else
          continue

    createLessImport = (path, setLessImport) ->
      lessImport = if setLessImport then "(less) " else ""
      "@import #{lessImport}\"#{path}\";\n"

    createStyleVendorFile = () ->
      vendorStyles = getVendorPaths(componentFiles.vendor.styles)
      outputString = ''
      for vendorStyle in vendorStyles
        outputString += createLessImport("#{options.vendorPathFromBasePath}/#{vendorStyle}", true)

      writeFile.sync "#{options.basePath}/#{options.styleVendorsFilename}", outputString

      grunt.log.ok "#{options.styleVendorsFilename} created."


    # seznam - všechny složky z A, M, O, T
    # ale vyjmu z něho věci z base dependencies.json
    # musim si vendory pamatovat bokem, protože na konci je dát na začátek


    # Vrátí seznam všech komponent.
    getComponentList = (exclude) ->

      components = []
      path = Finder.in(options.basePath).directory

      for directory in structure
        for component in Finder.in("#{options.basePath}/#{directory}").exclude(exclude).findDirectories()
          components.push component.replace(path + "/", "")

      return components

    parseDependenciesFile = (path, currentSection) ->
      filePath = "#{path}/#{options.jsonDependenciesFilename}"

      if fileExists.sync(filePath)

        file = require('../' + filePath) # nasosne si soubor...

        if file.vendor # ...abych ho mohl takhle hustě číst
          for value in file.vendor
            outputJson.vendor[value] = true

        delete file.vendor

        for type,components of file

          # kontrola vložení správné úrovně typu komponenty
          if type not in rules[currentSection]
            errorPath = path.replace(options.basePath + "/", "")
            grunt.log.warn "Soubor #{errorPath}/#{options.jsonDependenciesFilename} obsahuje špatné sekce."
            process.exit()

          for component in components
            outputJson[type][component] = true


    getComponentsPaths = (filename) ->
      output = []
      for type, helperBool in structure

        for component of outputJson[type]
          path = "#{options.basePath}/#{type}/#{component}/#{filename}"

          if fileExists.sync(path)
            output.push(path)
          else
            continue
      output

    # getVendorPaths()

    createScriptsDependenciesFile = () ->
      components = getComponentsPaths(componentFiles.components.scripts)

      for component,i in components
        components[i] = component.replace(options.basePath + '/', '')

      obj = {
        "vendors": getVendorPaths(componentFiles.vendor.scripts)
        "components": components
      }

      # Uložení výsledného JS
      writeFile.sync(options.outputScriptsFile, 'module.exports = (function () {\n    return ' + JSON.stringify(obj, null, '    ') + ';\n});')

      grunt.log.ok "main.js created."

      return

    createStyleDependenciesFile = () ->

      outputString = ''

      for keyT, type in structure
        for component of outputJson[keyT]

          for lessFile in componentFiles.lessDependencies
            xFile = "#{options.basePath}/#{keyT}/#{component}/#{lessFile}"
            if fileExists.sync(xFile)

              xFile = xFile.replace("#{options.basePath}/", "")
              outputString += createLessImport(xFile)
            else
              continue

        outputString += "\n"

      writeFile.sync "#{options.basePath}/#{options.lessDependenciesFilename}", outputString
      grunt.log.ok "#{options.lessDependenciesFilename} created."

      return


    init = () ->
      if options.basePath == ''
        grunt.log.warn 'Option `basePath` is required.'
        return false

      if !options.vendorPathFromBasePath
        grunt.log.warn 'Option `vendorPathFromBasePath` is required.'
        return false

      parseDependenciesFile(options.basePath, "base")

      excludedComponents = []
      for component in Object.keys outputJson.atoms
        excludedComponents.push "atoms/#{component}"

      components = getComponentList(excludedComponents)

      for component in components
        tmp = component.split "/"

        path = "#{options.basePath}/#{component}"

        parseDependenciesFile(path, tmp[0])

        outputJson[tmp[0]][tmp[1]] = true


      for type in structure
        for component,kokotina of outputJson[type]

          path = "#{options.basePath}/#{type}/#{component}"

          if not fileExists.sync(path + "/README.md")
            grunt.log.warn "Nebyla nalezena dokumentace README.md pro komponentu #{type}/#{component}."


      createStyleVendorFile()
      createStyleDependenciesFile()
      createScriptsDependenciesFile()

    init()

    return
  return
