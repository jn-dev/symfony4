'use strict'

module.exports = (grunt) ->
  grunt.registerMultiTask 'buildScripts', ':-)', ->
    options = @options(
      basePath: undefined
      dependenciesScriptsFile: undefined
      outputScriptsFile: undefined

      finalJsCallback: ''
    )

    writeFile = require('write')
    readFile = require('read-file')

    jsImport = require('../' + options.dependenciesScriptsFile)

    scripts = jsImport()

    outputString = ''
    for file in scripts.vendors
      buffer = readFile.sync(file)
      outputString += buffer.toString('utf8') + '\n'

    for file in scripts.components
      file = options.basePath + '/' + file
      buffer = readFile.sync(file)
      outputString += buffer.toString('utf8') + '\n'

    # Přidání callbacku na konec výsledného souboru
    outputString += options.finalJsCallback

    # Uložení výsledného JS
    writeFile.sync(options.outputScriptsFile, outputString)

    grunt.log.ok "scripts.js created."

    return

  return