# SKELETON APP

Pripraveny skeleton aplikace pro vyvoj.
Obsahuje sekci components, kde je pomoci atomic design ulozeny UI.

## Predpoklady
- mit lokalne instalovany grunt
- v dockeru spusteny projekt (url k docker-serveru)

## DEVELOPMENT

Pro spusteni developmentu jen nahodime
```
yarn dev
```

Vsechny zmeny z template jsou okamzite promitnuty na stranu prohlizece a podle potreby je proveden automaticky reload  stranky

**Upozornení:**
Na nove pridane soubory neni aplikovan *watch*. Porto je potreba v terminalu spustit ``yarn dev`` znovu.